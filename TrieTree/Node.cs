﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public class Node
    {

        const string Soglasn = "бвгджзйклмнпрстфхцчшшщ";
        public Dictionary<char, Node> Childs { get; set; } = new Dictionary<char, Node>();
        public int Count { get; set; }
        public void Add(string value)
        {
            if (value.Length == 0)
            {
                Count++;
                return;
            }
            char next = value[0];
            string remaining = value.Length == 1 ? "" : value.Substring(1);
            if (!Childs.ContainsKey(next))
            {
                Node node = new Node();
                Childs.Add(next, node);
            }
            Childs[next].Add(remaining);
        }

        public void PrintToTreeNode(TreeNode treeNode)
        {
            int i = 0;
            foreach (var keyValuePair in Childs)
            {
                treeNode.Nodes.Add(keyValuePair.Key.ToString());
                keyValuePair.Value.PrintToTreeNode(treeNode.Nodes[i]);
                i++;
            }
        }
        public Dictionary<string, int> Calculate(string start)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            if (Count > 0)
            {
                result.Add(start, Count);
            }
            foreach (var keyValuePair in Childs)
            {
                Dictionary<string, int> tmp = keyValuePair.Value.Calculate(start + keyValuePair.Key);
                foreach (var kvp in tmp)
                {
                    result.Add(kvp.Key, kvp.Value);
                }
            }
            return result;
        }
        
        public int WordsCheck(int needSogl)
        {
            int result = 0;
            int currentSogl;
            foreach (var keyValuePair in Childs)
            {
                if (Soglasn.Contains(keyValuePair.Key))
                {
                    currentSogl = 1;
                }
                else
                {
                    currentSogl = 0;
                }
                result += keyValuePair.Value.WordCount(needSogl, currentSogl);
            }
            return result;
        }

        public int WordCount(int needSogl, int currentSogl)
        {
            int wordCount = 0;

            if ((Count > 0) && (currentSogl == needSogl))
            {
                wordCount+= Count;
            }

            foreach (var keyValuePair in Childs)
            {
                if (Soglasn.Contains(keyValuePair.Key))
                {
                    currentSogl++;
                }
                int tmp = keyValuePair.Value.WordCount(needSogl, currentSogl);
                wordCount += tmp;
            }
            return wordCount;
        }

    }
}
