﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    class Tree
    {
        public Node Root { get; set; } = new Node();
        public void Add(string value)
        {
            Root.Add(value.ToLower());
        }
        public void PrintToTreeView(TreeView treeView)
        {
            if (Root != null)
            {
                treeView.Nodes.Add("");
                Root.PrintToTreeNode(treeView.Nodes[0]);
            }
        }
        public int WordsCheck(int needSogl)
        {
          return Root.WordsCheck(needSogl);
        }

        public void Clear()
        {
            Root = new Node();
        }

    }
}
