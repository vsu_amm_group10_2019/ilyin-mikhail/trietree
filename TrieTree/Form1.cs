﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public partial class MainForm : Form
    {
        Tree Tree = new Tree();
        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            string fileName = openFileDialog.FileName;
            string[] lines = File.ReadAllLines(fileName);
            Tree.Clear();
            foreach (string str in lines)
            {
                string[] words = str.Split(' ');
                foreach (string word in words)
                {
                    string tmp = word.Trim();
                    if (!string.IsNullOrEmpty(tmp))
                    {
                        Tree.Add(word);
                    }
                }
            }
            Redraw();
        }
        
        public void Redraw()
        {
            treeViewTrie.Nodes.Clear();
            Tree.PrintToTreeView(treeViewTrie);
            treeViewTrie.ExpandAll();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8)
            {
                e.Handled = true;
            }
        }

        private void buttonFind_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length != 0)
            {
                int needSogl = int.Parse(textBox1.Text);
                int i = Tree.WordsCheck(needSogl);
                labelOut.Visible = true;
                labelOut.Text = "Найдено слов:" + i;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Tree.Clear();
            Redraw();
        }
    }
}




